package strategies;

import java.util.Iterator;
import java.util.List;

import consumers.Server;
import consumers.Task;

public class ConcreteStrategyTime implements Strategy {


	@Override
	public void addTask(List<Server> servers, Task t, int timeL) {	
		int min = 1000000;
		for (Server s : servers) {
			int time = s.getWaitingPeriod().intValue();
			if (time < min)
				min = time;
		}
		int i=0;
		for (Server s : servers) {
			i++;
			int time = s.getWaitingPeriod().intValue();
			if(time == min) {
				System.out.println("adaug client: "+ t.getId()+ " la coada cu timp minim : " + i + " unde min= "+ min);
				s.addClient(t, timeL);
				break;
			}
		}

	}
}

package strategies;

import consumers.Task;
import java.util.List;
import consumers.Server;


public interface Strategy {
 public void addTask(List<Server> servers, Task t, int timeL);
}

package strategies;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import consumers.Server;
import consumers.Task;


public class Scheduler {
private List <Server> servers;
private int maxServ;
private int maxTasks;
private Strategy strategy = new ConcreteStrategyTime();

public Scheduler (int maxServ, int maxTasks) {
	this.maxServ = maxServ;
	this.maxTasks=maxTasks;
	servers = new ArrayList<>();
	Random r = new Random();
	int max = r.nextInt(100) + 100;
	for (int i = 0; i<maxServ; i++) {
		Server s = new Server(i+1, max);
		servers.add(s);
		Thread thread=new Thread(s);
	}
}
public void changeStrategy() {
        strategy = new ConcreteStrategyTime();
    
}

public void dispatchClient(Task client, int timeLimit) {
    strategy.addTask(servers, client, timeLimit);
}
public Server getCoada(int index) {
	int i=0;
	Server aux = new Server();
	for(Server s: servers) {
		i++;
		if(i==index)
			aux =s;
		
	}
	return aux;
	
}

public List<Server> getCozi() {
    return servers;
}

}

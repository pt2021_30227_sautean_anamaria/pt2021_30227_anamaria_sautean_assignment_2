package consumers;

public class Task implements Comparable<Task>{
	private int arrivalTime;
	private int processingTime;
	private int finishTime;
	private int id;

	public Task(int id, int arrivalTime, int processingTime, int waiting) {
		super();
		this.id = id;
		this.arrivalTime = arrivalTime;
		this.processingTime = processingTime;
		this.finishTime = arrivalTime + processingTime + waiting;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}

	public int getProcessingTime() {
		return processingTime;
	}

	public int getFinishTime() {
		return finishTime;
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public void setProcessingTime(int processingTime) {
		this.processingTime = processingTime;
	}

	public void setFinishTime(int finishTime, int waiting, int ok) {
		if (ok == 1)
			this.finishTime = this.arrivalTime + this.processingTime + waiting;

	}
	@Override
	public int compareTo(Task t) {
		int rezultat = this.arrivalTime - t.getArrivalTime();
		return rezultat;

	}

}

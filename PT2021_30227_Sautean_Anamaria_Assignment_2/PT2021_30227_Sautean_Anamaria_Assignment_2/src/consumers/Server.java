package consumers;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {
	private BlockingQueue<Task> clients;
	private AtomicInteger waitingPeriod;
	private int id;

	public Server(int id, int max) {
		this.id = id;
		clients = new ArrayBlockingQueue<>(max);
		waitingPeriod = new AtomicInteger(0);
	}
	public Server () {
		
	}

	public void addClient(Task client, int timeLimit) {
		int wait = waitingPeriod.intValue(), proces = client.getProcessingTime();
		if (client.getProcessingTime() <= timeLimit - wait) {
			//if (client != null)
				clients.add(client);
			client.setFinishTime(0, wait, 1);
			waitingPeriod.addAndGet(client.getProcessingTime());
		}

	}

	public void remove() throws InterruptedException {
		while (clients.size() == 0) {
			wait();
		}
		clients.remove();
	}

	@Override
	public synchronized void run() {
		int ok = 0;
		while (clients.isEmpty() == false || getWaitingPeriod().intValue() != 0) {
			if (this.clients.isEmpty() == false)
				ok = 1;
			try {
				Thread.sleep(1000);
				if (ok == 1) {

					Task t = clients.element();
					if (t != null) {
						try {
							Thread.sleep(t.getProcessingTime());
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						clients.remove();
						waitingPeriod.addAndGet(-t.getProcessingTime());
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public BlockingQueue<Task> getClients() {
		return clients;
	}

	public AtomicInteger getWaitingPeriod() {
		return waitingPeriod;
	}

	public void setWaitingPeriod(AtomicInteger waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}



}

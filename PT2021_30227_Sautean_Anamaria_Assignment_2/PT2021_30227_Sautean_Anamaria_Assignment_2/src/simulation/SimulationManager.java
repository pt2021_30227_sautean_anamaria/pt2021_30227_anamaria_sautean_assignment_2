package simulation;

import strategies.Scheduler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import consumers.Task;
import consumers.Server;

public class SimulationManager implements Runnable {
	private int timeLimit;
	private int maxProc;
	private int minProc;
	private int maxArr;
	private int minArr;
	private int noServ;
	private int noTask;
	private Scheduler scheduler;
	private SimulationFrame frame;
	private List<Task> generatedClients = new ArrayList<>();

	public SimulationManager(SimulationFrame frame, int timeLimit, int clientsNo, int serversNo, int maxProc,
			int minProc, int maxArr, int minArr) {
		this.frame = frame;
		this.noTask = clientsNo;
		this.maxProc = maxProc;
		this.minProc = minProc;
		this.noServ = serversNo;
		this.timeLimit = timeLimit;
		this.maxArr = maxArr;
		this.minArr = minArr;
		scheduler = new Scheduler(noServ, noTask);
		for (Server s : scheduler.getCozi()) {
			Thread thread = new Thread(s);
			thread.start();
		}
		generateNRandomTasks();
		for (Task t : generatedClients) {
			System.out.println(
					"id:  " + t.getId() + " arrival: " + t.getArrivalTime() + " process: " + t.getProcessingTime());
		}

	}

	public SimulationManager(SimulationFrame fr) {
		this.frame = fr;
		this.frame.addListener(new Listener());

	}

	class Listener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			if (e.getSource() == frame.getStart()) {

				SimulationManager sim;
				sim = new SimulationManager(frame, Integer.parseInt(frame.getLimit()),
						Integer.parseInt(frame.getClientsNo()), Integer.parseInt(frame.getServersNo()),
						Integer.parseInt(frame.getMaxProc()), Integer.parseInt(frame.getMinProc()),
						Integer.parseInt(frame.getMaxArr()), Integer.parseInt(frame.getMinArr()));

				// System.out.println(Integer.parseInt(frame.getLimit()));

				Thread thread = new Thread(sim);
				thread.start();

			}
		}

	}

	public void generateNRandomTasks() {
		int n, i = 0, pTime, aTime;
		Random rand = new Random();
		Random rand2 = new Random();
		while (i < noTask) {
			// System.out.println(maxArr + " and " + minArr);
			pTime = rand.nextInt(maxProc - minProc) + minProc;
			aTime = rand2.nextInt(maxArr - minArr) + minArr;
			Task t = new Task(i + 1, aTime, pTime, 0);
			this.generatedClients.add(t);
			i++;
		}
		Collections.sort(generatedClients);
	}

	public void display(List<Task> tasks, int noTask, int currT, Scheduler sch) {
		for (Task t : tasks) {
			// frame.getLogArea().append("ceva");
			frame.getLogArea().append(String.valueOf(t.getId()) + " ");
		}
		frame.getLogArea().append("\n");
		for (int i = 0; i < sch.getCozi().size(); i++) {
			int ind = i + 1;
			frame.getLogArea().append("C " + ind + " : ");
			Server s = sch.getCoada(i + 1);
		    System.out.println("nr bun de clienti: " + s.getClients().size()+ " la coada : "+ ind);
			for (Task t : s.getClients()) {
				//System.out.println();
				frame.getLogArea()
						.append(String.valueOf(t.getId()) + " ( arrival: " + String.valueOf(t.getArrivalTime())
								+ " process: " + String.valueOf(t.getProcessingTime() + " )    "));
			}
			frame.getLogArea().append("\n");
		}
	}

	public int condition(Scheduler sc) {
		int ok=1;
	
		for (Server s : sc.getCozi()) {
			if(s.getClients().size()!=0)
				ok=0;
			}
		if (ok==1) {
			if (generatedClients.size()!=0)
				ok=0;
		}
			
		return ok;
		
	}
	public void fisier(int current,List<Task> tasks, int noTask, int currT, Scheduler sch, FileWriter f ) {
		try {
			f.write("Time: " + current + " \n");
			f.write("Waiting : \n");
			for (Task t : tasks) {
				f.write(String.valueOf(t.getId()) + " ");
			}
			f.write("\n");
			for (int i = 0; i < sch.getCozi().size(); i++) {
				int ind = i + 1;
				f.write("C " + ind + " : ");
				Server s = sch.getCoada(i + 1);
				for (Task t : s.getClients()) {
					f.write(String.valueOf(t.getId()) + " ( arrival: " + String.valueOf(t.getArrivalTime())
									+ " process: " + String.valueOf(t.getProcessingTime() + " )    "));
				}
				f.write("\n");
				//f.close();
			}
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		int currentTime = 1, emp = 0, nr = 0, i, ok = 0, start=0;
		try {
			FileWriter f = new FileWriter("C:\\\\Users\\\\RogStrix\\\\eclipse-workspace\\\\PT2021_30227_Sautean_Anamaria_Assignment_2\\\\file.txt");
		
		List<Task> auxiliar = new ArrayList<Task>();

		while (currentTime <= timeLimit && ok == 0) {
			frame.getLogArea().append("Time: " + currentTime + " \n");
			frame.getLogArea().append("Waiting : \n");
			List<Task> aux = new ArrayList<Task>();
			// displayFields();
			//System.out.println(currentTime);

			for (Task t : this.generatedClients) {
				if (t.getArrivalTime() == currentTime) {
					//System.out.println("intra");
					aux.add(t);
				}

			}

			
			for (Task t : aux) { // parcurg lista de clienti disponibili
				scheduler.dispatchClient(t, timeLimit); // adauga client
				start=1;
				auxiliar.add(t);
				generatedClients.remove(t);
			}
			
			for (Server s : scheduler.getCozi()) {
				for (Task t : s.getClients()) {
					t.setProcessingTime(t.getProcessingTime() - 1);
					s.setWaitingPeriod(new AtomicInteger(s.getWaitingPeriod().intValue()-1));
					break;
				}
			}
			for (Server s : scheduler.getCozi()) {
				for (Iterator<Task> it = s.getClients().iterator(); it.hasNext();) {
					Task t = it.next();
					//System.out.print(t.getId() + " ");
					if (t.getProcessingTime() == 0) {
						s.getClients().remove(t);
						break;
					}
				}
			}
			display(generatedClients, noTask, currentTime, scheduler);
			fisier(currentTime, generatedClients, noTask, currentTime, scheduler, f);
			
			
			if (condition(scheduler) == 1 && start ==1)
				ok = 1;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			currentTime++;
		}
		if (ok == 1)
			frame.getLogArea().append("S-a incheiat repartizarea \n");
		else
		frame.getLogArea().append("S-a terminat timpul \n");
		
		f.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SimulationFrame f = new SimulationFrame();
		SimulationManager sim = new SimulationManager(f);
		// f.getLogArea().append("nsjhabdhja");

	}
}

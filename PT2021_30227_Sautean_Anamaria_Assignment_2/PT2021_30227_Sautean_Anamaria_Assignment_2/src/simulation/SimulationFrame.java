package simulation;


import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;

public class SimulationFrame extends JFrame{
	

	private JLabel clnmb=new JLabel("Nr Clienti:");
	private JLabel snmb=new JLabel("Nr Cozi");
	private JLabel timel=new JLabel("Time limit of arrival");
	private JLabel maxt=new JLabel("Max Proc Time");
	private JLabel mint=new JLabel("Min proc Time");
	private JButton start=new JButton("Go");
	private JLabel maxa=new JLabel("Max Arrival Time");
	private JLabel mina=new JLabel("Min Arrival Time");
	private JTextArea logArea=new JTextArea(800,800);
	private JScrollPane logpane=new JScrollPane(logArea);
	private JTextField clno=new JTextField(5);
	private JTextField sno=new JTextField(5);
	private JTextField tlim=new JTextField(5);
	private JTextField mxt=new JTextField(5);
	private JTextField mnt=new JTextField(5);
	private JTextField mxta=new JTextField(5);
	private JTextField mna=new JTextField(5);
	
	public SimulationFrame()
	{
		JFrame f=new JFrame();    
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(1000,1000);
		f.setVisible(true);
		
		JPanel p1=new JPanel();
		p1.setLayout(new GridLayout(0,2));
		
		
		logArea.setFont(new Font("Helvetica",Font.BOLD,12));
		
		clnmb.setFont(new Font("Helvetica",Font.BOLD,10));
		p1.add(clnmb);
		clno.setBackground(Color.pink);
		p1.add(clno);
		snmb.setFont(new Font("Helvetica",Font.BOLD,10));
		p1.add(snmb);
		sno.setBackground(Color.pink);
		p1.add(sno);
		timel.setFont(new Font("Helvetica",Font.BOLD,10));
		p1.add(timel);
		tlim.setBackground(Color.pink);
		p1.add(tlim);
		maxt.setFont(new Font("Helvetica",Font.BOLD,10));
		p1.add(maxt);
		mxt.setBackground(Color.pink);
		p1.add(mxt);
		
		mint.setFont(new Font("Helvetica",Font.BOLD,10));
		p1.add(mint);
		mnt.setBackground(Color.pink);
		p1.add(mnt);
		
		maxa.setFont(new Font("Helvetica",Font.BOLD,10));
		p1.add(maxa);
		mxta.setBackground(Color.pink);
		p1.add(mxta);
		
		mina.setFont(new Font("Helvetica",Font.BOLD,10));
		p1.add(mina);
		mna.setBackground(Color.pink);
		p1.add(mna);
		
		start.setBackground(Color.pink);
		p1.add(start);
		
        p1.setBackground(Color.pink);
		p1.setLayout(new GridLayout(0,2));
		
		
		JPanel p4=new JPanel();
		p4.setLayout(new BoxLayout(p4,BoxLayout.Y_AXIS));
		p4.add(logpane);
		
		JPanel p3=new JPanel();
		p3.setLayout(new BoxLayout(p3,BoxLayout.Y_AXIS));
		p3.add(p1);
		p3.add(p4);
		
		f.setContentPane(p3);
		f.getContentPane().setBackground(Color.pink);
		f.setVisible(true);
		
		
	}
	  public void resetArea() {
	        logArea.setText("");
	        logpane.repaint();
	        logArea.repaint();
	        logpane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	        logpane.setPreferredSize(new Dimension(1700, 800));
	        logpane.setMinimumSize(new Dimension(0, 0));
	    }
	

   void addListener(ActionListener listen)	
	{
       start.addActionListener(listen);
	}


    public String getClientsNo()
    {
    	return clno.getText();
    }
    public String getServersNo()
    {
    	return sno.getText();
    }
    public String getLimit()
    {
    	return tlim.getText();
    }
    public String getMaxProc()
    {
    	return mxt.getText();
    }
    public String getMinProc()
    {
    	return mnt.getText();
    }
    public String getMaxArr()
    {
    	return mxta.getText();
    }
    public String getMinArr()
    {
    	return mna.getText();
    }
    public JButton getStart()
	{
		return start;
	}
	public JTextArea getLogArea() {
		return logArea;
	}
	
    
  
   
 
    
}
